let theTableElementNow; //Table des informations météo de toutes les villes, actualisées heure par heure.
let theTableElementTomorrow; //Table des informations météo de toutes les villes, dans un jour à la même heure.

let chart; //Définition du graphique. Il s'agit d'une variable globale pour conserver les températures des villes précédentes et pouvoir les comparer.
let datav = []; //Données (label, data) du graphique qui sont actualisées à l'ajout d'une nouvelle ville.
datav.datasets = [];
let nb_villes = 0; //Nombre de villes affichées, utile pour dessiner le graphique.

const apiURL1 = 'https://api.open-meteo.com/v1/meteofrance?latitude=';
const apiURL2 = '&longitude=';
const apiURL3 = '&hourly=temperature_2m,apparent_temperature,precipitation,weathercode,windspeed_10m,winddirection_10m&timezone=Europe%2FBerlin';
//Fragments de l'url de l'api météo, à assembler avec des coordonnées (longitude, latitude) pour obtenir un url valide.

const latLannion = 48.73; //Latitude de la ville de Lannion.
const longLannion = -3.46; //Longitude de la ville de Lannion.
const lannion = "Lannion"; //Label utilisé pour la ville de Lannion (seule entrée du tableau par défaut).

const apiURL = apiURL1+latLannion+apiURL2+longLannion+apiURL3;
//Url de l'api météo correspondant à la ville de Lannion.

const apiGeocodage = 'https://api-adresse.data.gouv.fr/search/?q=';
//Début de l'url de l'api de localisation. Le nom de la ville doit être ajouté à la fin.


window.addEventListener('DOMContentLoaded',init); //Attendre la fin de chargement du DOM pour lancer le script.

function init(){
    recuperer_meteo(apiURL,lannion); //On cherche à afficher par défaut les informations météo de la ville de Lannion. Les coordonnées de la ville sont déjà connues.
    //let btnElmt = document.getElementById("btn");
    //btnElmt.addEventListener("click",addNewCity());
}

function recuperer_meteo(url,city){
    //Retourne la réponse de l'api météo en format json. Les coordonnées sont présentes dans l'url "api" et le nom de la ville correspondante est dans "city".
    theTableElementNow = document.getElementById('theTable1'); //On identifie notre table des données météo d'aujourd'hui à la balise <table id="theTable1"> du fichier html.
    theTableElementTomorrow = document.getElementById('theTable2'); //On identifie notre table des données météo d'aujourd'hui à la balise <table id="theTable2"> du fichier html.
    function obtenir(){//Fonction qui encapsule l'api fetch.
    fetch(url) //On effectue une requête à l'api "url".
        .then((response)=>{
            return response.text()})
        .then((data)=>{addLineToTheTableNow(data,city);
                     addLineToTheTableTomorrow(data,city);
                     makechart(data,city)})
     };
     obtenir();
}

function addNewCity(){
    //Ajout d'une nouvelle ville dans les 2 tables et le graphique.
    const ville = document.getElementById('textzone').value; //On récupère la chaine de caractères entrée dans la barre de texte.
    newApiGeocodage = apiGeocodage + ville; //On construit l'url de la requête vers l'api du gouvernement avec l'url de base de la recherche concaténée à la ville dont on souhaite obtenir les coordonnées.
    fetch(newApiGeocodage) //On effectue la requête pour obtenir les coordonnées de la ville.
        .then((response)=>{
            return response.text()})
        .then((data)=>{meteo_personnalisee(data)})
}

function addNewSuggestions(){
    //Ajout de villes à suggérer pour aider l'utilisateur à choisir la ville.
    const ville1 = document.getElementById('textzone').value; //On récupère la chaine de caractères entrée dans la barre de texte.
    newApiGeocodage = apiGeocodage + ville1; //On construit l'url de la requête vers l'api du gouvernement avec l'url de base de la recherche concaténée à la ville dont on souhaite obtenir les coordonnées.
    fetch(newApiGeocodage) //On effectue la requête pour obtenir les labels des villes qui correspondent le plus à la chaine de caractères présente dans la barre de texte.
        .then((response)=>{
            return response.text()})
        .then((data)=>{recuperer_villes(data)})
}

function meteo_personnalisee(data){
    //Cette fonction récupère le json donnant le nom des villes et leurs coordonnées respectives et traite ce dernier pour préparer la requête météo.
    const response2 = JSON.parse(data); //Transforme la chaine de caractères json en objet JavaScript.
    const city = response2.features[0].properties.label; //Récupère la première ville en réponse, celle qui correspond le plus à la ville demandée (celle dont le score est le plus élevé).
    longNew = response2.features[0].geometry.coordinates[0]; //Obtenir la longitude de cette ville.
    latNew = response2.features[0].geometry.coordinates[1]; //Obtenir la latitude de cette ville.
    newAPI = apiURL1+latNew+apiURL2+longNew+apiURL3; //Construction de l'url de la requête météo, les chaines sont concaténées de façon à placer les coordonnées au bon endroit.
    recuperer_meteo(newAPI,city); //Lancement de la requête météo avec l'url ainsi construit. On garde aussi le nom de la ville.
}

function recuperer_villes(data){
    //Cette fonction traite le json de suggestion des villes pour construire la liste de suggestion déroulante au niveau de la barre de texte.
    const response3 = JSON.parse(data); //Conversion d'une chaine de caractères json en document JavaScript.
    n = response3.features.length; //Le nombre de suggestions peut varier, il faut connaître ce nombre.
    var options = ''; //Chaine de caractères qui contient les suggestions.
    for(var i=0;i<n;i++){
        options += '<option value="' + response3.features[i].properties.label + '" />'; //On construit la chaine de caractères correspondante en repectant la syntaxe des suggestions.
    };
    document.getElementById('returnedCities').innerHTML = options; //On affiche la liste des suggestions au niveau de la barre de texte <input list ="returnedCities">.
}

//Les deux fonctions suivantes sont quasiment identiques. Seule la première est complètement commentée.

function addLineToTheTableNow(data,city){
    //Ajouter les informations météo de la ville au tableau météo ("Now" donc il s'agit du tableau de la météo actuelle).
    var now = new Date(); //Obtenir la date actuelle.
    nowHour = now.getHours(); //Obtenir l'heure de la date actuelle (les informations sont actualisées toutes les heures).
    const response = JSON.parse(data); //Conversion d'une chaine de caractères json en document JavaScript.

    //Dans les lignes qui suivent, les informations météo vont être récupérées à partir du document js response.

    const temperature = response.hourly.temperature_2m[nowHour].toFixed(1) + ' ' + response.hourly_units.temperature_2m;
    //Récupération de la température à un chiffre après la virgule avec son unité correspondante (le °C).
    const weather_condition = weather(response.hourly.weathercode[nowHour]);
    //Récupération du code météo.
    const apparent_temperature = response.hourly.apparent_temperature[nowHour].toFixed(1) + ' ' + response.hourly_units.apparent_temperature;
    //Récupération de la température ressentie à un chiffre après la virgule avec son unité correspondante (le °C).
    const precipitation = precipSurUneJournee(response.hourly.precipitation,0).toFixed(1) + ' ' + response.hourly_units.precipitation;
    //Récupération des précipitations heure par heure à un chiffre après la virgule. La somme des précipiations se fait sur une journée, ici la journée actuelle (d'où le paramètre 0).
    //Les précipiations sont données avec leur unité, ici le "mm".
    const windspeed = response.hourly.windspeed_10m[nowHour].toFixed(1) + ' ' + response.hourly_units.windspeed_10m + ' ';
    //Récupération de la vitesse du vent à un chiffre après la virgule avec son unité (le "km/h").
    const winddirection = response.hourly.winddirection_10m[nowHour].toFixed(0) + response.hourly_units.winddirection_10m;
    //Récupération de l'orientation du vent à l'unité près avec son unité (le "°").

    //Dans les lignes qui suivent, les lignes et les cellules du tableau sont créées.

    const newWeatherReport = document.createElement('tr');
    //Création d'une nouvelle ligne.

    const newCity = document.createElement('td');
    const newTemperature = document.createElement('td');
    const newCondition = document.createElement('td');
    const newPrecipitation = document.createElement('td');
    const newApparentTemperature = document.createElement('td');
    const newWind = document.createElement('td');
    //Création de 6 cases correspondant à chaque information météo (5 au total, la vitesse et l'orientation du vent étant regroupées) + le nom de la ville.
    
    newCity.innerText = city;
    newTemperature.innerText = temperature;
    newCondition.innerText = weather_condition;
    newApparentTemperature.innerText = apparent_temperature;
    newPrecipitation.innerText = precipitation;
    newWind.innerText = windspeed + winddirection;
    //Les conditions météo sont affichées dans leurs cases respectives.

    newWeatherReport.appendChild(newCity);
    newWeatherReport.appendChild(newTemperature);
    newWeatherReport.appendChild(newCondition);
    newWeatherReport.appendChild(newApparentTemperature);
    newWeatherReport.appendChild(newPrecipitation);
    newWeatherReport.appendChild(newWind);
    //Les cases sont toutes ajoutées sur une même ligne.

    theTableElementNow.appendChild(newWeatherReport);
    //La ligne est ajoutée à la table.
}

function addLineToTheTableTomorrow(data,city){
    var now = new Date();
    nowHour = now.getHours();
    const response = JSON.parse(data);

    //La récupération des données se fait pour le lendemain à la même heure d'où 'heure+24' dans les lignes qui suivent.
    const temperature = response.hourly.temperature_2m[nowHour+24].toFixed(1) + ' ' + response.hourly_units.temperature_2m;
    const weather_condition = weather(response.hourly.weathercode[nowHour+24]);
    const apparent_temperature = response.hourly.apparent_temperature[nowHour+24].toFixed(1) + ' ' + response.hourly_units.apparent_temperature;
    const precipitation = precipSurUneJournee(response.hourly.precipitation,1).toFixed(1) + ' ' + response.hourly_units.precipitation;
    const windspeed = response.hourly.windspeed_10m[nowHour+24].toFixed(1) + ' ' + response.hourly_units.windspeed_10m + ' ';
    const winddirection = response.hourly.winddirection_10m[nowHour+24].toFixed(0) + response.hourly_units.winddirection_10m;

    const newWeatherReport = document.createElement('tr');
    const newCity = document.createElement('td');
    const newTemperature = document.createElement('td');
    const newCondition = document.createElement('td');
    const newPrecipitation = document.createElement('td');
    const newApparentTemperature = document.createElement('td');
    const newWind = document.createElement('td');

    newCity.innerText = city;
    newTemperature.innerText = temperature;
    newCondition.innerText = weather_condition;
    newApparentTemperature.innerText = apparent_temperature;
    newPrecipitation.innerText = precipitation;
    newWind.innerText = windspeed + winddirection;

    newWeatherReport.appendChild(newCity);
    newWeatherReport.appendChild(newTemperature);
    newWeatherReport.appendChild(newCondition);
    newWeatherReport.appendChild(newApparentTemperature);
    newWeatherReport.appendChild(newPrecipitation);
    newWeatherReport.appendChild(newWind);

    theTableElementTomorrow.appendChild(newWeatherReport);
}

function weather(code){
    //Cette fonction retourne la chaine de caractères correspondant au commentaire du code météo passé en argument.
    //Les commentaires sont donnés sur le site de l'api météo (en anglais, ils ont été traduits ici).
    summary = code;
    switch(code){
        case 0: summary += ' : Temps clair';
        break;
        case 1: summary += ' : Nombreuses éclaircies';
        break;
        case 2: summary += ' : Partiellement couvert';
        break;
        case 3: summary += ' : Couvert';
        break;
        case 45: summary += ' : Brouillard';
        break;
        case 48: summary += ' : Brouillard givrant';
        break;
        case 51: summary += ' : Bruine légère';
        break;
        case 53: summary += ' : Bruine modérée';
        break;
        case 55: summary += ' : Bruine importante';
        break;
        case 56: summary += ' : Bruine verglaçante légère';
        break;
        case 57: summary += ' : Bruine verglaçante importante';
        break;
        case 61: summary += ' : Pluie légère';
        break;
        case 63: summary += ' : Pluie modérée';
        break;
        case 65: summary += ' : Pluie importante';
        break;
        case 66: summary += ' : Pluie verglaçante légère';
        break;
        case 67: summary += ' : Pluie verglaçante importante';
        break;
        case 71: summary += ' : Chutes de neige légères';
        break;
        case 73: summary += ' : Chutes de neige modérées';
        break;
        case 75: summary += ' : Chutes de neige importantes';
        break;
        case 77: summary += ' : Neige en grains';
        break;
        case 80: summary += ' : Averses légères';
        break;
        case 81: summary += ' : Averses modérées';
        break;
        case 82: summary += ' : Violentes averses';
        break;
        case 85: summary += ' : Tempête de neige modérée'
        break;
        case 86: summary += ' : Violente tempête de neige';
        break;
        case 95: summary += ' : Orage';
        break;
        case 96: summary += ' : Orage léger avec grêle';
        break;
        case 99: summary += ' : Orage important avec grêle';
        break;
    };
    return summary;
}

function precipSurUneJournee(precip,mode){
    //Les précipitations sont données heure par heure, ce qui n'est pas très intuitif.
    //Cette fonction effectue la somme des précipitations sur une journée.
    heure = 0; //Heure depuis le minuit de la journée courante (début des données).
    volume = 0; //Volume des précipiations.
    if(mode==1){ //Pour les précipitations de la journée d'après, décaler l'horaire de 24 heures.
        heure = heure + 24;
    };
    for(var i = 0;i<=23;i++){ //Faire la somme des précipiations sur une journée.
        volume = volume + precip[heure+i];
    };
    return volume; //Retourner le volume des précipitations.
}

function makechart(data,city){
    //Construire le graphique à partir du json des données météo et du nom de la ville d'étude.
    nb_villes = nb_villes + 1; //Nombre de villes affichées sur la page.
    response4 = JSON.parse(data); //Conversion de la chaine de caractères json en document JavaScript.
    const ctx = document.getElementById('myChart'); //Correspondance entre le graphique et le canvas qui le contient dans le fichier html : <canvas id="myChart">
    var n = response4.hourly.temperature_2m.length; //Nombre de températures à afficher.
    var values = new Array(n); //Tableau des températures.
    for(var i = 0;i<n;i++){
        values[i]= {x:response4.hourly.time[i],y: response4.hourly.temperature_2m[i]};
        //Ce tableau contient les coordonnées x,y du graphique : en abscisse le temps et en ordonnée les températures.
    };
    colors = ['blue','red','cyan','orange','purple','gold','grey']; //Liste de couleurs différentes pour distinguer les villes.
    datav.datasets[nb_villes-1]={
        label: city, //Nom de la ville dont on affiche les températures en label.
        data: values, //Tableau des températures en données.
        borderColor: colors[(nb_villes-1)%7], //La couleur du trait doit varier selon la liste de couleurs prédéfinie.
        backgroundColor: '#FFFFFF', //Couleur d'arrière-plan, ici du blanc.
        tension: 0.3, //La courbe qui arrondit entre deux points au lieu d'être une ligne droite.
    };
    if(nb_villes>1){
        chart.destroy(); //Détruire le graphique précédent pour pouvoir en construire un nouveau un peu plsu loin.
        };;
    chart = new Chart(ctx, { //(Re)construire le graphique selon la syntaxe préconisée par chart.js
        type: 'line', //Construire une ligne continue (type du grahique).
        data: {
            datasets: datav.datasets, //Les données du graphique sont constituées des différentes courbes et de leurs coordonées respectives (définies au-dessus).
        }, 
    });    
    chart.update(); //Mise à jour du graphique car des changements ont été affectués.
}